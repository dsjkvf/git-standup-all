git-standup-all
===============

## About

`git-standup-all` is a little script that uses [`standup`](https://github.com/kamranahmedse/git-standup) git extension to report the latest activities for the repositories in the current directory. Errors, non-git subdirectories and repositories that hadn't had any activity for the requested time period are ignored.

## Dependencies

Naturally, [`git standup`](https://github.com/kamranahmedse/git-standup).

Also, by default the script utilizes [BSD `date`](https://www.freebsd.org/cgi/man.cgi?date) (which is also the date command macOS uses). However, in order to switch to [GNU `date`](http://man7.org/linux/man-pages/man1/date.1.html), one should simply switch the commenting for the corresponding sections of the script.

## Usage

If run without parameters, checks the activity for the latest 24 hours. If a number N is supplied, checks the activity on the `today - N` day. If a special parameter `0` is supplied, checks the activity for the current calendar day only.
